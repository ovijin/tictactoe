<?php

/**
 * Description of FrontController
 *
 * @author George Jinga <ovidiu.jinga@gmail.com>
 */
abstract class FrontController {
    
    /** \Request $request */
    private $request;

    /**
     * 
     * @param \Request $request
     */
    public function __construct(\Request $request) {
        $this->request = $request;
    }

    /**
     * @return \Request
     */
    public function getRequest() {
        return $this->request;
    }

    public function index() {
        printf("Hello from here %s", __METHOD__);
    }

    /**
     * 
     * @param type $request
     */
    public function setRequest($request) {
        $this->request = $request;
    }

    /**
     * @param string $model
     * @return \model
     */
    public function loadModel($model) {

        // check the existence of the model file
        $model_file = 'application/models/' . $model . '.php';
        if (!file_exists($model_file)) {
            die('ERROR: Model file does not exist! Create <b>' . $model . '.php</b> file in <b>application/model/</b> folder.');
        }

        // load the file
        require_once($model_file);

        // create an instance of the model
        return new $model;
    }

    /**
     * @param string $view
     * @param array $params
     */
    public function loadView($view, $params = array()) {
        if (is_array($params) && count($params) > 0) {
            // convert the parameters array in variables
            extract($params, EXTR_PREFIX_SAME, 'mvc');
        }

        // check the existence of the view file
        $viewFile = 'application/views/' . $view . '.php';
        if (!file_exists($viewFile)) {
            die('ERROR: View file does not exist! Create <b>' . $view . '.php</b> file in <b>application/view/</b> folder.');
        }

        // load the view file
        require_once($viewFile);
    }

    
}
