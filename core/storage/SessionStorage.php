<?php

/**
 * Description of SessionStorage
 *
 * @author George Jinga
 */
class SessionStorage extends Storage {

    const SESSION_NAME = 'tictactoe';

    public function save() {
        if (session_id() === '') {
            session_start();
        }
        $_SESSION[self::SESSION_NAME] = $this->getData();
    }
    
    public function load() {
        if (session_id() === '') {
            session_start();
        }
        if (isset($_SESSION[self::SESSION_NAME])) {
            $this->setData($_SESSION[self::SESSION_NAME]);
        }
    }
}
