<?php

/**
 * basic class for handling requests
 */
class Request {

    public function get($key, $defaultValue = null) {
        if (!array_key_exists($key, $_GET)) {
            return $defaultValue;
        }

        return filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING);
    }

    public function post($key, $defaultValue = null) {
        if (!array_key_exists($key, $_POST)) {
            return $defaultValue;
        }

        return filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
    }

    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return string
     */
    public function server($key, $defaultValue = null) {
        if (!array_key_exists($key, $_SERVER)) {
            return $defaultValue;
        }

        return filter_input(INPUT_SERVER, $key, FILTER_SANITIZE_STRING);
    }

    /**
     * @param integer $elementNumber
     * @param string $defaultValue
     * @return string
     */
    public function path($elementNumber, $defaultValue = '') {
        $requestUri = $this->server('REQUEST_URI');
        $elements = explode('/', trim($requestUri, '/'));
        return isset($elements[$elementNumber]) && !empty($elements[$elementNumber]) ? $elements[$elementNumber] : $defaultValue;
    }
}
