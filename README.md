George's TicTacToe
===

This is a basic implementation of the classic tic-tac-toe game that I did some years ago and now I decided to re-factor it in an OOP manner. 

It's still far from finished but step by step, it's taking shape, the ultimate goal being to use WebSockets and use AJAX just as a fallback for the browsers that don't offer support for WebSockets. 

It's built on top of the LAMP stack, even though it doesn't use any database for storage, yet. One idea is to add Redis support.

Roadmap
-------

* Highest priority: complete unit testing
* Simplify and improve AI
* Implement [Ratchet](http://socketo.me/)
* ...
* Profit!