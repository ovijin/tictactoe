<?php

/**
 * Description of Game
 *
 * @author George Jinga
 */
class GameController extends FrontController {

    /** @var Game $game */
    private $game;

    /**
     * @return Game
     */
    public function getGame() {
        return $this->game;
    }

    /**
     * @param Game $game
     * @return \GameController
     */
    public function setGame(Game $game) {
        $this->game;
        return $this;
    }

    /**
     * @param \Request $request
     */
    public function __construct(\Request $request) {
        parent::__construct($request);
        $storage = $this->getStorage();
        $this->game = $this->gameInit($storage);
    }

    /**
     * @param Storage $storage
     * @return Game
     */
    private function gameInit(Storage $storage) {
        $board = $this->getBoardModel($storage);
        $game = $this->getGameModel($board, $storage);

        return $game;
    }

    /**
     * @return \SessionStorage
     */
    private function getStorage() {
        return new SessionStorage();
    }

    /**
     * @param Storage $storage
     * @throws Exception
     * @return Board
     */
    private function getBoardModel(Storage $storage) {
        $board = $this->loadModel('board');
        $board->setStorage($storage);
        try {
            $board->initBoard();
        } catch (Exception $exception) {
            printf("Exception thrown: %s", $exception->getMessage());
            exit;
        }

        return $board;
    }

    /**
     * @return Game
     */
    private function getGameModel($board, $storage) {
        $game = $this->loadModel('game');
        $game->setStorage($storage);
        $game->setBoard($board);
        $game->initGame();

        return $game;
    }

    /**
     * @return type
     */
    private function getClickedCell() {
        $clickedCell = $this->getRequest()->post('id');
        return array(
            'line' => floor(intval($clickedCell) / 10),
            'column' => intval($clickedCell) % 10
        );
    }

    public function playerMove() {
        $game = $this->getGame();
        $clickedCell = $this->getClickedCell();
        $game->registerMove($clickedCell['line'], $clickedCell['column'], 2);

        $response = $this->getResponse($game);
        echo json_encode($response);
    }

    /**
     * @param Game $game
     * @return array
     */
    private function getResponse($game) {
        $response = array('message' => '');

        if ($game->checkPlayerWin()) {
            // check if user wins with this move
            $response['message'] = 'You win';
            $game->resetGame();
        } elseif ($game->checkDraw()) {
            // otherwise check if it's a draw
            $response['message'] = "It's a draw";
            $game->resetGame();
        } else {
            // otherwise get a computer move
            $computerMove = $game->getComputerMove();
            $cell = $computerMove['cell'];
            $game->registerMove($cell['line'], $cell['column'], 5);
            $response['message'] = $computerMove['message'];
            $response['cell'] = $cell['line'] . $cell['column'];
        }

        return $response;
    }

    public function index() {
        $this->loadView("index");
    }

    public function playAgain() {
        $this->getGame()
            ->resetGame();
        echo json_encode(array('message' => 'Go again'));
    }

}
